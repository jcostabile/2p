# -*- coding: utf-8 -*-

# Load packages
import cv2
import imutils

import networkx as nx
import numpy as np
import astropy.units as u

from matplotlib import path
from scipy.interpolate import UnivariateSpline
from sklearn.neighbors import NearestNeighbors
from scipy.signal import convolve2d as conv2
from fil_finder import FilFinder2D


# Overall code to process HDF5 files and dropped images, used in 'update_behavior_hdf5.py
def tailPositions(img, chakra, pxSmoothingArea=(10,10), testing=False,
                  contourArea=[(150,200), (150,600), (550,600), (550,200)]):

  # Apply mask to image
  t = np.array([[chakra[0], chakra[1]], [int(chakra[0]*0.5), img.shape[1]], 
                [int(chakra[0]*1.5), img.shape[1]], [chakra[0], chakra[1]]])
  imask = np.zeros(img.shape, dtype=np.uint8)
  imask = cv2.bitwise_not(cv2.drawContours(imask, [t], -1, 255, -1))
  imgM = cv2.bitwise_and(img, img, mask=imask)
  imgM = cv2.morphologyEx(imgM, cv2.MORPH_OPEN, cv2.getStructuringElement(cv2.MORPH_RECT,(5,5)))

  # Threshold image
  imgT = cv2.threshold(imgM.copy(), 3, 255, 0)[1] # Threshold image
  imgTC = cv2.morphologyEx(imgT, cv2.MORPH_CLOSE, cv2.getStructuringElement(cv2.MORPH_RECT,(20,20)))

  # Find largest contours of thresheld image
  contours = cv2.findContours(imgTC.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)
  cnts = imutils.grab_contours(contours)
  # Ensure that contour is where it is suppose to be
  p = path.Path(contourArea)
  cnts[:] = [cnt for cnt in cnts if any(p.contains_points(cnt.squeeze()))]
  # Then find the largest one   
  c = sorted(cnts, key=cv2.contourArea, reverse=True)[0]

  # Exclude all pixels not contained within the largest contour
  mask = np.zeros(imgTC.shape, dtype=np.uint8)
  cv2.drawContours(mask, [c], -1, 255, -1)
  imgTCM = cv2.bitwise_and(imgTC, imgTC, mask=mask)

  # Skeletonize masked tail image
  tailMask = extractTailPosition(imgTCM, chakra, 8, 0, 'ZHANGSUEN', False)
  # And remove skeleton branches
  fil = FilFinder2D(tailMask, distance=250*u.pc, mask=tailMask)
  fil.create_mask(border_masking=True, verbose=False, use_existing_mask=True)
  fil.medskel(verbose=False)
  fil.analyze_skeletons(branch_thresh=40*u.pix, skel_thresh=10*u.pix, prune_criteria='length')
  tailPositions = np.nonzero(tailMask)  ## Convert to coordinates

  # Massage points
  decimalTailPositions = decimalPrecision(img, tailPositions, pxSmoothingArea[0], pxSmoothingArea[1])
  points = pointSorting(decimalTailPositions, chakra+[0,10])  ## Tail position sorting

  # Append central body point ('chakra') to points
  points = np.append(points, chakra[None,:], axis=0)
  # Insert midpoint between last entry in 'points' and 'chakra'
  points = np.insert(points, -1, (2/3)*points[-2] + (1/3)*points[-1], 0)
  points = np.insert(points, -1, (1/2)*points[-2] + (1/2)*points[-1], 0)

  tailLength, distance = tailMeasures(points)  ## Linear length along the line
  # Get tail angles and associated information
  pointsFitted, tailAngles = computeTailAngles(points, distance)

  # Transform angles (x), take difference (d)
  xAngles = np.append(3/2*np.pi, tailAngles)
  dAngles = np.append(0, np.diff(xAngles))
  dinds = np.nonzero(np.abs(dAngles) > np.pi)[0]
  dAngles[dinds] = (xAngles[dinds]-2*np.sign(dAngles[dinds])*np.pi) - xAngles[dinds-1]
  dAngles = -1*dAngles

  # Plot for fun
  if testing:
    pl.figure(); pl.imshow(img)
    pl.plot(*pointsFitted.T, '.w')
    pl.plot(*chakra, 'om')
    pl.xlim([150, 550]); pl.ylim([600, 350])
    #pl.title('File index {0}, Image index {1}, Tail length {2:.3f}'.format(dind, num, tailLength))
    pl.title('Tail length {0:.3f}'.format(tailLength))

  if (tailLength < 95 or tailLength > 250):
    return None, None, tailLength
  else:
    return pointsFitted, dAngles, tailLength


# Apply Huang, et al. (2013) method to smooth tail coordinates
def decimalPrecision(img, pos, i=1, j=1):

  decimalTailPositions = np.zeros(np.shape(pos))

  for n,(x,y) in enumerate(zip(pos[0], pos[1])):
    # Create i-by-j coordinates around one tail position coordinate (x,y)
    iRange, jRange = np.arange(-1*i, i+1), np.arange(-1*j, j+1)
    xi = np.tile(iRange, 2*i+1) + x
    yj = np.repeat(jRange, 2*j+1) + y

    # Find sum of i-by-j pixels around one tail position coordinate (x,y)
    imgIdx = np.array([xi, yj])
    xySum = np.take(img, np.ravel_multi_index(imgIdx, img.shape)).astype(np.float).sum()

    # Find x decimal precision
    xSum = 0.0
    for ii in iRange:
      xSumIdx = np.array([np.repeat(ii+x, 2*j+1), jRange+y])
      xSum += np.float(ii)*np.take(img, np.ravel_multi_index(xSumIdx, img.shape)).astype(np.float).sum()
    xPrime = np.float(x) + xSum/xySum

    # Find y decimal precision
    ySum = 0.0
    for jj in jRange:
      ySumIdx = np.array([iRange+x, np.repeat(jj+y, 2*i+1)])
      ySum += np.float(jj)*np.take(img, np.ravel_multi_index(ySumIdx, img.shape)).astype(np.float).sum()
    yPrime = np.float(y) + ySum/xySum

    # Insert into output vector
    decimalTailPositions[:,n] = [xPrime, yPrime]

  return decimalTailPositions


# Compute the tail angles from the sorted tail positions ("sortedPoints")
def computeTailAngles(sortedPoints, distance, fittedSize=21):

  # Build a list of the spline function, one for each dimension:
  splines = [UnivariateSpline(distance, coords, k=3, s=.5) for coords in sortedPoints.T]

  # Computed the spline for the asked distances:
  alpha = np.linspace(0, 1, fittedSize)
  # alpha = np.delete(alpha, 1)  ## remove the second element, first and second points don't deviate much
  sortedPointsFitted = np.flip(np.vstack([spl(alpha) for spl in splines]).T, axis=0).astype('int')

  # Compute tail angles
  tailAngles = np.arctan2(*np.diff(np.flip(sortedPointsFitted,1).T))
  tailAngles[tailAngles < 0] += 2*np.pi 

  return (sortedPointsFitted, tailAngles)


# Tail-point sorting function
def pointSorting(coords, bodyPart):

  if coords.shape[-1] > 3:
    # Append 'bodyPart' to closest end of 'coords'
    distFromBP = np.linalg.norm(bodyPart-np.fliplr(coords.T), axis=-1) 

    # Separate into 'x' and 'y' vectors
    if distFromBP[0] < distFromBP[-1]:
      y, x = coords
    else:
      y, x = np.fliplr(coords) 

    # Add 'bodyPart' as first point, to sort other based off known head position
    x = np.insert(x, 0, bodyPart[0])
    y = np.insert(y, 0, bodyPart[1])
    points = np.c_[x, y]

    # Use some graph theory (credit StackOverflow) to sort coordinates
    clf = NearestNeighbors(n_neighbors=3).fit(points)
    G = clf.kneighbors_graph(mode='distance')
    # T = nx.from_scipy_sparse_matrix(G)
    # order = list(nx.dfs_preorder_nodes(T, source=0))[1:]

    # Loop to order points by shortest distance
    cols = np.arange(1, G.toarray().shape[1])
    order = np.full(cols.shape, np.nan)
    i,rc = (0,0)
    while np.isnan(order).any():
      row = G.toarray()[i]  ## Distances from one position
      d = np.ma.masked_where(row==0, row)[cols]  ## Masked to ignore zeros
      order[rc] = cols[np.argmin(d)]  ## Minimum distance
      cols = np.setdiff1d(cols, order[rc])  ## Remove position from search
      i = int(order[rc])  ## Inspect the next position
      rc += 1  ## Update "rowcounter"

    # Sort points based off 'order'
    xx = x[order.astype(int)]
    yy = y[order.astype(int)]
    pointSorted = np.flip(np.c_[xx,yy],0)  # furthest length is "0" distance

    return pointSorted
  else:  ## Not enough coords to sort
    return np.array([[1,2], [3,4]])


# Determine tail positions by skeletonizing the outline of the larva
def extractTailPosition(threshImg, bodyPart, resolutionAdjustment, frameNumber, thinningMethod, plotting=False):

  # Create mask to separate tail into two components
  headMask = np.zeros(threshImg.shape, dtype=np.uint8)
  cv2.circle(headMask, tuple(np.array(bodyPart, dtype=int)), int(3*resolutionAdjustment), 255, -1)

  # Thinning operation
  if thinningMethod == 'GUOHALL':
    wholeSkeleton = cv2.ximgproc.thinning(threshImg.copy(), thinningType=cv2.ximgproc.THINNING_GUOHALL)
  elif thinningMethod == 'ZHANGSUEN':
    wholeSkeleton = cv2.ximgproc.thinning(threshImg.copy(), thinningType=cv2.ximgproc.THINNING_ZHANGSUEN)

  if np.sum(wholeSkeleton != 0) > 10:
    # Apply mask 'headMask' to 'wholeSkeleton'
    twoSkeletonSegs = cv2.bitwise_and(wholeSkeleton.copy(), wholeSkeleton.copy(), mask=cv2.bitwise_not(headMask))	
    # Find largest segment of 'twoSkeletonSegs'
    throwAwayMask = np.zeros(threshImg.shape, dtype=np.uint8)
    throwAwayContours = cv2.findContours(twoSkeletonSegs.copy(), cv2.RETR_LIST, cv2.CHAIN_APPROX_NONE)  
    throwAwayCnts = imutils.grab_contours(throwAwayContours)
    throwAwayC = sorted(throwAwayCnts, key=np.shape, reverse=True)[0]
    cv2.drawContours(throwAwayMask, [throwAwayC], -1, 255, -1) 
    imgThin = cv2.bitwise_and(twoSkeletonSegs.copy(), throwAwayMask)

    # Plotting
    if plotting:
      # Create bladder mask
      bodyMaskColor = cv2.cvtColor(headMask.copy(), cv2.COLOR_GRAY2RGB)
      bodyMaskColor[:,:,2] = 0

      # Create thinning mask
      imgThinColor = cv2.cvtColor(wholeSkeleton.copy(), cv2.COLOR_GRAY2RGB) 
      imgThinColor[:,:,1] = 0;
      imgThinMask = cv2.bitwise_not(wholeSkeleton.copy())

      # Plotting scratch code (at the moment)
      imgColor = cv2.cvtColor(threshImg.copy(), cv2.COLOR_GRAY2RGB)
      img1BG = cv2.bitwise_and(imgColor.copy(), imgColor.copy(), mask = threshImg)

      dst = imgColor.copy()
      dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = cv2.bitwise_not(headMask.copy()))
      dst = cv2.add(dst.copy(), bodyMaskColor)
      dst = cv2.bitwise_and(dst.copy(), dst.copy(), mask = imgThinMask)
      dst = cv2.add(dst, imgThinColor)

      # Add some text
      text = 'Frame ' + str(frameNumber)
      cv2.putText(dst, text, (10,50), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (255, 255, 255), 1)

      cv2.imshow('Tail tracking', cv2.resize(dst, (480,480))); cv2.waitKeyEx(0); cv2.destroyAllWindows()

    return imgThin
  else:
    return np.zeros(threshImg.shape, dtype=np.uint8)


# Determine tail length and positions along it
def tailMeasures(pointSorted):
  cumDistance = np.cumsum(np.sqrt(np.sum(np.diff(pointSorted, axis=0)**2, axis=1)))
  tailLength = cumDistance[-1]
  # print('Total distance: {0:.3f}'.format(tailLength))
  distance = np.insert(cumDistance, 0, 0)/cumDistance[-1]

  return (tailLength, distance)


# Remove branching from tail mask
def removeBranches(tailMask):
  # Set intensity range = (0,1)
  tmN = np.zeros(tailMask.shape)   
  tmN = cv2.normalize(tailMask, tmN, 0, 1, cv2.NORM_MINMAX)

  # Look for position connected by more than two
  dot = cv2.getStructuringElement(cv2.MORPH_RECT, (3,3))
  dot[1,1] = 0
  filtered = conv2(tmN.astype(np.float), dot, mode='same')
  filtered[filtered <= 2] = 0

  # Separated branches ('D' = "disconnected")
  tmND = cv2.bitwise_and(tmN, cv2.bitwise_not(cv2.bitwise_and(tmN, filtered.astype(np.uint8)))) 
  __, labels = cv2.connectedComponents(tmND)
  labelUnique, labelCounts = np.unique(labels.flatten(), return_counts=True)
  longestBranchLabel = labelUnique[np.argsort(labelCounts)[-2]]
  labelMask = labels.copy()
  labelMask[labelMask != longestBranchLabel] = 0

  return cv2.bitwise_and(tailMask, tailMask, mask=labelMask.astype(np.uint8))


# Find pixel in thresheld image that are connected only by one corner
def removeSingleCornerPixels(threshImg):
  # Create hit-and-miss kernels
  kul = np.array(([1, -1,-1], 
    [-1, 1, 0], 
    [-1, 0, 0]), dtype="int") 
  kll = np.array(([-1, 0, 0], 
    [-1, 1, 0], 
    [1, -1, -1]), dtype="int") 
  kur = np.array(([-1, -1, 1], 
    [0, 1, -1], 
    [0, 0, -1]), dtype="int") 
  klr = np.array(([0, 0, -1], 
    [0, 1, -1], 
    [-1, -1, 1]), dtype="int")

  # Apply kernels to incoming thresheld image
  imgul = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kul) 
  imgll = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kll) 
  imgur = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kur) 
  imglr = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, klr) 

  # Union of the kernel images
  imgKU = cv2.bitwise_or(imgul, cv2.bitwise_or(imgll, cv2.bitwise_or(imgur, imglr)))

  # Subtracted the kernel images from thresheld
  threshSub = cv2.bitwise_and(threshImg.copy(), cv2.bitwise_not(imgKU))

  # Remove spurious pixels and return
  return removeSpuriousPixels(threshSub.copy())


# Remove spurious pixels from thresheld image
def removeSpuriousPixels(threshImg):
  # Create hit-and-miss kernels
  kspur = np.array(([-1, -1, -1], 
    [-1, 1, -1], 
    [-1, -1, -1]), dtype="int")
  # Remove spurious pixels and return
  imgSpur = cv2.morphologyEx(threshImg.copy(), cv2.MORPH_HITMISS, kspur) 
  return cv2.bitwise_and(threshImg.copy(), cv2.bitwise_not(imgSpur))

