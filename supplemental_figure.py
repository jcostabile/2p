# -*- coding: utf-8 -*-
import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib as mpl
import matplotlib.pyplot as pl

from sklearn.metrics import r2_score
from virtual_swim_behavior import load_bhvr

mpl.rcParams['pdf.fonttype'] = 42
mpl.rcParams['ps.fonttype'] = 42

if __name__ == "__main__":
  # Load free swimming behavior file
  bpath = '/media/jamie/2tb-sandisk/2P/bhvr-analysis/total_2rep_raw_modelData.bhvr'
  B, _, _, umap = load_bhvr(bpath)

  # Convert units pixels-->mm, radians-->degrees
  B['dposition'] = B['dposition'] / 8 ## free swimming behavior rig resolution: 8 px/mm
  B['dheading'] = np.rad2deg(B['dheading'])
  B['rolling_sum_tail'] = np.rad2deg(B['rolling_sum_tail'])
  B['vigor'] = B['vigor'] * (180/np.pi)

  # Create trendlines
  tl_disp = np.poly1d(np.polyfit(B['vigor'], B['dposition'], 1))
  tl_angle = np.poly1d(np.polyfit(B['rolling_sum_tail'], B['dheading'], 1))

  # Plot vigor vs displacement
  fig, ax = pl.subplots(1,2)
  ax[0].plot(B['vigor'], B['dposition'], '.', color='gray')
  ax[0].plot(B['vigor'], tl_disp(B['vigor']), '-k', linewidth=2)
  ax[0].text(0.05, 0.95, f"$\\rho = {np.corrcoef(B['vigor'], B['dposition'])[0,1]:0.3f}$",
             transform=ax[0].transAxes, fontsize=10, verticalalignment='top')

  ax[0].set_xlabel(r'Vigor ($\degree$/s)', fontweight='bold')
  ax[0].set_ylabel(r'$\Delta$displacement (mm)', fontweight='bold')
  ax[0].set_xticks([0,500,1000,1500,2000]); ax[0].set_yticks([0,5,10,15])

  # Plot rolling sum tail vs heading change
  ax[1].plot(B['rolling_sum_tail'], B['dheading'], '.', color='gray')
  ax[1].plot(B['rolling_sum_tail'], tl_angle(B['rolling_sum_tail']), '-k', linewidth=2)
  ax[1].text(0.05, 0.95, f"$\\rho = {np.corrcoef(B['rolling_sum_tail'], B['dheading'])[0,1]:0.3f}$",
             transform=ax[1].transAxes, fontsize=10, verticalalignment='top')

  ax[1].set_xlabel(r'$\Sigma$ (cumulative tail angle) ($\degree$)', fontweight='bold')
  ax[1].set_ylabel(r'$\Delta$heading ($\degree$)', fontweight='bold')
  ax[1].set_xticks([-6000,-3000,0,3000,6000]); ax[1].set_yticks([-180,-90,0,90,180])

