# -*- coding: utf-8 -*-

# Load libraries
import numpy as np
import matplotlib.pyplot as pl; pl.ion()


def run_interpolation(_tailinfo, _allowed=5, _tidx=(5,-1)):

  # Prepare data to update
  tailinfo2 = np.full(_tailinfo.shape, np.nan)  ## matrix for interpolated tail positions
  inds = np.nonzero(~np.isnan(_tailinfo[:,:,0]))
  tailinfo2[inds] = _tailinfo[inds]

  pos_idx = [i for i in range(_tailinfo.shape[1])][_tidx[0]:_tidx[1]]  ## last position index list
  cta = np.cumsum(_tailinfo[:,:,0], axis=1)

  # Necessary to have position prior, to calculate angles; hence 'sub'
  for seg in range(0, pos_idx[-1]+1):  ## the '+1' includes the data for last index
    bogusFrames = np.nonzero(np.isnan(cta[:,seg]))[0]

    if bogusFrames.size > 0:
      # Identify sequentially (<_allowed) missing frames
      dBF = np.diff(bogusFrames)
      separator = np.nonzero(dBF > 1)[0] + 1
      splitFrames = np.split(bogusFrames, separator)
      toFix = [np.insert(f, (0, f.size), [f[0]-1, f[-1]+1])
                for f in splitFrames if f.shape[0] <= _allowed]
      # disrepair = [f for f in splitFrames if f.shape[0] > _allowed]

      for frames in toFix:
        # Interpolate missing positions over designated frames
        missX, missY = interpolate_over_frames(_tailinfo, frames, seg)
        # Insert into '_tailinfo' and 'tailinfo2'
        #_tailinfo[frames[1:-1], seg, 1] = tailinfo2[frames[1:-1], seg, 1] = missX
        #_tailinfo[frames[1:-1], seg, -1] = tailinfo2[frames[1:-1], seg, -1] = missY
        tailinfo2[frames[1:-1], seg, 1] = np.round(missX)
        tailinfo2[frames[1:-1], seg, -1] = np.round(missY)

  # Calculate angles and update
  angles = calc_angles(tailinfo2)
  finds = np.nonzero(np.logical_and(np.isnan(tailinfo2[:,:,0]), ~np.isnan(tailinfo2[:,:,1])))
  for f1,f2 in zip(finds[0], finds[1]):
    tailinfo2[f1,f2,0] = angles[f1,f2]

  return tailinfo2


def interpolate_over_frames(_tailinfo, _frames, _seg):
  # Extract known positions
  center = np.mean(_tailinfo[_frames,0,1:], axis=0)
  xy = _tailinfo[_frames[::(len(_frames)-1)]][:, _seg, 1:]

  # Calculate radius from "center" (i.e., swim bladder) to missing positions
  r = np.linalg.norm(xy-center, axis=-1)
  # Interpolate
  radii = np.linspace(r[0], r[1], len(_frames), axis=-1)[1:-1]

  # Angle between swim bladder and known tail positions
  theta0 = np.arctan2(xy[0,1]-center[1], xy[0,0]-center[0])  ## (*)
  thetaN = np.arctan2(*(xy[1] - center)[::-1])  ## (*)these are equivalent
  # Interpolate
  theta = np.linspace(theta0, thetaN, len(_frames), axis=-1)[1:-1]

  # Calculate missing position values
  missX = center[0] + radii*np.cos(theta)
  missY = center[1] + radii*np.sin(theta)

  return (missX, missY)


def calc_single_angle(_tailinfo2, _f):
  vectors = np.diff(_tailinfo2[_f, :, 1:], axis=0)
  origin_angles = np.insert(np.arctan2(vectors[:,1], vectors[:,0]), 0, -np.pi/2)
  angles = np.insert(np.diff(origin_angles), 0, 0)
  angles[angles < -np.pi-0.015] = 2*np.pi + angles[angles < -np.pi-0.015]
  angles[angles > np.pi-0.015] = angles[angles > np.pi-0.015] - 2*np.pi
  #angles[np.abs(angles)>np.pi] = np.arctan(np.sin(angles[np.abs(angles)>np.pi]) /
  #                               np.cos(angles[np.abs(angles)>np.pi]))  ## handles discontinuity
  return angles


def calc_multi_angles(_tailinfo2, _f):
  vectors = np.diff(_tailinfo2[_f, :, 1:], axis=1)
  origin_angles = np.insert(np.arctan2(vectors[:,:,1], vectors[:,:,0]), 0, -np.pi/2, axis=1)
  angles = np.insert(np.diff(origin_angles), 0, 0, axis=1)
  angles[angles < -np.pi-0.015] = 2*np.pi + angles[angles < -np.pi-0.015]
  angles[angles > np.pi-0.015] = angles[angles > np.pi-0.015] - 2*np.pi

  return angles


def calc_angles(_tailinfo2):
  vectors = np.diff(_tailinfo2[:,:,1:], axis=1)
  origin_angles = np.insert(np.arctan2(vectors[:,:,1], vectors[:,:,0]), 0, -np.pi/2, axis=1)
  angles = np.insert(np.diff(origin_angles), 0, 0, axis=1)
  angles[angles < -np.pi-0.015] = 2*np.pi + angles[angles < -np.pi-0.015]
  angles[angles > np.pi-0.015] = angles[angles > np.pi-0.015] - 2*np.pi

  return angles


def tail_plotting(_tailinfo, _tailinfo2, _frames, _tidx):
  for n, frame in enumerate(_frames[1:-1]):
    # A figure for each missing frame
    pl.figure()

    # Plot known
    pl.plot(*_tailinfo[_frames[0], _tidx[0]:_tidx[1], 1:].T, '.-m')
    pl.plot(*_tailinfo[_frames[-1], _tidx[0]:_tidx[1], 1:].T, '.-c')

    # Plot info from missing frames
    # inds = np.nonzero(~np.isnan(_tailinfo2[frame,:,1]))[0]
    pl.plot(*_tailinfo[frame, _tidx[0]:_tidx[1], 1:].T, '.-b')
    pl.plot(*_tailinfo2[frame, _tidx[0]:_tidx[1], 1:].T, 'x-k')
    pl.title('Missing frame {}'.format(n+1), fontweight='bold')
    pl.legend(['Start', 'End', 'Known', 'Unknown'], frameon=False)
    pl.xticks([],[]); pl.yticks([],[])


def seg_plotting(_tailinfo, _tailinfo2, _frames, _tidx0, _seg):
  for n, frame in enumerate(_frames[1:-1]):
    # A figure for each missing frame
    pl.figure()

    # Plot known
    pl.plot(*_tailinfo[_frames[0], _tidx0:(_seg+1), 1:].T, '.-m')
    pl.plot(*_tailinfo[_frames[-1], _tidx0:(_seg+1), 1:].T, '.-c')

    # Plot info from missing frames
    # inds = np.nonzero(~np.isnan(_tailinfo2[frame,:,1]))[0]
    pl.plot(*_tailinfo[frame,  _tidx0:(_seg+1), 1:].T, '.-b')
    pl.plot(*_tailinfo2[frame, _tidx0:(_seg+1), 1:].T, 'x-k')
    pl.title('Missing frame {}'.format(n+1), fontweight='bold')
    pl.legend(['Start', 'End', 'Known', 'Unknown'], frameon=False)


# This portion of the code was originally used to generate missing tail information in the EXTENDED
# frame window, i.e., when the bout window is increased to 70 frames as input to the ANN, but now
# used to help create data from dropped frames (i.e., dropped tail positions)
def interpolate_using_avg_coord_diff(_tailinfo, _indexes, _num):
  if len(_indexes) == 0:
    ang, crd = (_tailinfo[...,0], _tailinfo[...,1:])
    if np.isnan(ang).any():  ## only change ones with NaNs
      rej = np.nonzero(np.isnan(ang))  ## find NaNs
      rejFrames = np.unique(rej[0])  ## specifically the frames with NaNs
      rejTail = ang[rejFrames]
      rejCoords = crd[rejFrames]
      for rf,rt,rc in zip(rejFrames, rejTail, rejCoords):  ## iterate over each frame in bout
        if np.sum(~np.isnan(rt)) > _num:  ## use data contained in the frame if 15 points exist
          # Calculate average coordinate change within single frame
          frame_avg_diff_rc = np.round(np.nanmean(np.diff(rc, axis=0), axis=0))

          # Fill in NaNs in the coordinates variable, then calculate angles from result
          for g,urc in enumerate(rc):
            if (urc == 0).any():
              rc[g] = rc[g-1] + frame_avg_diff_rc

          # Calculate angles from coordinates and only replace the new ones
          new_angles = calc_angle_from_coords(rc)
          for g,urt in enumerate(rt):
            if np.isnan(urt).any():
              rt[g] = new_angles[g]

          # Update '_tailinfo'
          _tailinfo[rf] = np.concatenate((rt[:,np.newaxis],rc), axis=1)

  else:
    mdn_coords = np.nanmedian(_tailinfo[...,1:], axis=0)
    avg_angles = calc_angle_from_coords(mdn_coords)
    for i in _indexes:  ## iterate over each bout
      ang, crd = (_tailinfo[i,...,0], _tailinfo[i,...,1:])
      if np.isnan(ang).any():  ## only change ones with NaNs
        rej = np.nonzero(np.isnan(ang))  ## find NaNs
        rejFrames = np.unique(rej[0])  ## specifically the frames with NaNs
        rejTail = ang[rejFrames]
        rejCoords = crd[rejFrames]
        for rf,rt,rc in zip(rejFrames, rejTail, rejCoords):  ## iterate over each frame in bout
          if np.sum(~np.isnan(rt)) > _num:  ## use data contained in the frame if 5 points exist
            # Calculate average coordinate change within single frame
            frame_avg_diff_rc = np.round(np.nanmean(np.diff(rc, axis=0), axis=0))

            # Fill in NaNs in the coordinates variable, then calculate angles from result
            for g,urc in enumerate(rc):
              if np.isnan(urc).any():
                rc[g] = rc[g-1] + frame_avg_diff_rc

            # Calculate angles from coordinates and only replace the new ones
            new_angles = calc_angle_from_coords(rc)
            for g,urt in enumerate(rt):
              if np.isnan(urt).any():
                rt[g] = new_angles[g]

            # Update '_tailinfo'
            _tailinfo[i[rf]] = np.concatenate((rt[:,np.newaxis],rc), axis=1)
          else:  ## use overall averages 'avg_dstail' and 'avg_Rds' to update
            # Update '_tailinfo'
            _tailinfo[i[rf]] = np.concatenate((avg_angles[:,np.newaxis], mdn_coords), axis=1)

  return _tailinfo

def calc_angle_from_coords(_coords):
  vectors = np.diff(_coords, axis=0)
  origin_angles = np.insert(np.arctan2(vectors[:,1], vectors[:,0]), 0, -np.pi/2)
  angles = np.insert(np.diff(origin_angles), 0, 0)
  angles[angles < -np.pi-0.015] = 2*np.pi + angles[angles < -np.pi-0.015]
  angles[angles > np.pi-0.015] = angles[angles > np.pi-0.015] - 2*np.pi
  #angles[np.abs(angles)>np.pi] = np.arctan(np.sin(angles[np.abs(angles)>np.pi]) /
  #                               np.cos(angles[np.abs(angles)>np.pi]))  ## handles discontinuity
  return angles


