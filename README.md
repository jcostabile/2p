# Two-photon behavior pipeline

Software to process behavior data from the two-photon rig contained in the HDF5 files.

## Installation
Create clone the directory
  `git clone https://bitbucket.org/jcostabile/2p.git`

Enter directory and create conda environment
  `cd ./2P`
  `conda create --name [name] --file environment.yml`

## File list
+ `update_behavior_hdf5.py` (run file): (1) analyze dropped frames contained in the .tif stacks, 
(2) convert data's orientation to match standard (i.e., larva imaged from the top with head pointing 
in same direction as the top of the image sensor)

+ `virtual_swim_behavior.py` (run file): (1) generates an HDF5 containing processed behavior data 
for each HDF5 from the two-photon microscope [should be ran after `update_behavior_hdf5.py`], (2) 
converts tail movements from head-fixed space to "free-moving" virtual space 

+ `image_processing.py` (utility file): for image processing in `update_behavior_hdf5.py`

+ `helper_functions.py` (utility file): for data extraction in `virtual_swim_behavior.py`

+ `experiment.py` (utility file): the two-photon rig data object, used in both run files

+ `behavior_video_2p.py` (utility file): creates video of behaving larval zebrafish <u>extended 
experiment required</u>

+ `environment.yml` (utility file): conda environment file

### &rarr; Run code according to description in script's comments section


