# -*- coding: utf-8 -*-
import os
import glob
import umap
import subprocess

import numpy as np
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as pl

from sklearn.decomposition import PCA
from sklearn.preprocessing import StandardScaler

from virtual_swim_behavior import load_bhvr


# Label dataframe
def LABELS(_bf, _bh, _umapf, _umaph):
  session = ['free' for _ in range(_umapf.shape[0])]
  session.extend(['fixed' for _ in range(_umaph.shape[0])])
  reg_bouts = np.concatenate((_bf['regular_bout'].values, _bh['regular_bout'].values, [1]))

  # Create dataframe for labels
  _df = pd.DataFrame(data=np.concatenate((np.asarray(session)[:,None], reg_bouts[:,None]), axis=1),
                           columns=['session', 'regular_bout'])
  _df['regular_bout'] = _df['regular_bout'].astype(int)

  return _df


# NN results directionality dataframe
def QUADS(_umapf, _umaph, _labels):

  # Calculate displacement and heading quadrants
  results = np.concatenate((_umapf[:,1:5], _umaph[:,1:5]), axis=0)
  quadD = np.arctan2(results[:,1], results[:,0]) - np.pi/2
  quadD[quadD < -np.pi] += 2*np.pi
  quadH = np.arctan2(results[:,-2], results[:,-1]) - np.pi/2
  quadH[quadH < -np.pi] += 2*np.pi

  # Convert quads to binning
  quadrants = [-np.pi, -3/4*np.pi, -1/4*np.pi, 1/4*np.pi, 3/4*np.pi, np.pi]
  qD = np.digitize(quadD, quadrants, right=True)
  qH = np.digitize(quadH, quadrants, right=True)
  qD[qD==5] = 1; qH[qH==5] = 1

  # Funnel into dataframe
  _df = pd.DataFrame(data=np.concatenate((qD[:,None], qH[:,None]), axis=1),
                     columns=['displacement', 'heading'])
  # Convert integer label to string label
  _df['displacement'] = _df['displacement'].map({1: 'back', 4: 'left', 3: 'straight', 2: 'right'})
  _df['heading'] = _df['heading'].map({1: 'back', 2: 'left', 3: 'straight', 4: 'right'})

  return _df


def pca_plot():
  # Separate dataframe by session and bout type
  rfix = df.query('session == "fixed" and regular_bout == 1')
  ifix = df.query('session == "fixed" and regular_bout == -1')
  rfree = df.query('session == "free" and regular_bout == 1')
  ifree = df.query('session == "free" and regular_bout == -1')

  fig, ax = pl.subplots(1,2)
  sns.scatterplot(data=df.query("session == 'free'").sample(frac=0.5), x='pc0', y='pc1',
                  hue='displacement', hue_order=quad_labels.values(), s=10, alpha=0.1,
                  palette="tab10", ax=ax[0])
  sns.scatterplot(data=df.query("session == 'fixed'").sample(frac=0.5), x='pc0', y='pc1',
                  hue='displacement', hue_order=quad_labels.values(), s=10, alpha=0.1,
                  palette="tab10", ax=ax[1])
  ax[0].set_xlim([-80,80]); ax[0].set_ylim([-80,80]); ax[0].set_title("Free", fontweight='bold')
  ax[1].set_xlim([-80,80]); ax[1].set_ylim([-80,80]); ax[1].set_title("Fixed", fontweight='bold')

  # Plot PCA
  pl.figure()
  pl.plot(rfix['pc0'], rfix['pc1'], '.', color='blue')
  pl.plot(ifix['pc0'], ifix['pc1'], '.', color='orange')
  pl.plot(rfree['pc0'], rfree['pc1'], '.', color='green')
  pl.plot(ifree['pc0'], ifree['pc1'], '.', color='red')
  pl.ylabel('Principle component 2', fontweight='bold')
  pl.xlabel('Principle component 1', fontweight='bold')
  pl.legend(['Fixed-Reg', 'Fixed-Irr', 'Free-Reg', 'Free-Irr'], frameon=False)


def boutVideo(tail, bout, saveDir, angleAx=0, vidFR=30):
  '''
  Creates a video of tail movement WITH angle heatmap inlayed
  '''

  # Shift angles depending on axis
  if angleAx == 0:
    angle_shift = 0
  elif angleAx == 1:
    angle_shift = -np.pi/2

  # Dimensions for tail angle arrows
  l, hw, hl = (0.5, 0.2, 0.2)
  # Plottig region -- currently hard-coded
  acceptableArea = [[-20, 20], [-35, -2]]

  # Prepare plot
  fig = pl.figure(999)
  ax = fig.add_subplot(111)
  # Prepare inlay
  subpos = [0.025,-0.01,0.95,0.4]
  subax1 = add_subplot_axes(ax, subpos)
  subax1.set_xticks([])
  subax1.set_yticks([])
  subax1.imshow(tail[bout,...,angleAx].T)

  # Create video
  for i, (tl,ag) in enumerate(zip(tail[bout,...,2:], tail[bout,...,angleAx])):
      # Plot tail coordinates
      ax.plot(*tl.T, '.k', markersize=4)
      ax.set_ylim(acceptableArea[1]); ax.set_yticks([])
      ax.set_xlim(acceptableArea[0]); ax.set_xticks([])
      # Plot tail angles
      for t,a in zip(tl[:-1], ag[1:]):
          ax.arrow(*t, (l)*np.cos(a+angle_shift), (l)*np.sin(a+angle_shift),
                   head_width=hw, head_length=hl, fc=None, ec='magenta', linewidth=2)
      ax.set_title('Frame ' + str(i), fontsize=10, color='red', fontweight='bold', loc='left')

      # Track tail movement with vertical line
      subax1.axvline(x=i, color='red')

      # Save plot as .png and clear axis
      pl.savefig(os.path.join(saveDir, 'file%02d.png' % i))
      subax1.lines[0].remove()
      ax.cla()
  pl.close()

  # Create video from images
  subprocess.call(['ffmpeg', '-framerate', str(vidFR), '-i', os.path.join(saveDir, 'file%02d.png'),
                   '-r', str(30), '-pix_fmt', 'yuv420p', os.path.join(saveDir,
                   'bout_' + str(bout) + '_ax' + str(angleAx) + '.mp4')])
  for file_name in glob.glob(os.path.join(saveDir, '*.png')):
      os.remove(file_name)


def add_subplot_axes(ax,rect,axisbg='w'):
  '''
  Generates a subaxis inlayed into the designated axis 'ax'
  '''

  fig = pl.gcf()
  box = ax.get_position()
  width = box.width
  height = box.height
  inax_position  = ax.transAxes.transform(rect[0:2])
  transFigure = fig.transFigure.inverted()
  infig_position = transFigure.transform(inax_position)
  x = infig_position[0]
  y = infig_position[1]
  width *= rect[2]
  height *= rect[3]  # <= Typo was here
  subax = fig.add_axes([x,y,width,height],facecolor=axisbg)  # matplotlib 2.0+
  #subax = fig.add_axes([x,y,width,height],axisbg=axisbg)
  x_labelsize = subax.get_xticklabels()[0].get_size()
  y_labelsize = subax.get_yticklabels()[0].get_size()
  x_labelsize *= rect[2]**0.5
  y_labelsize *= rect[3]**0.5
  subax.xaxis.set_tick_params(labelsize=x_labelsize)
  subax.yaxis.set_tick_params(labelsize=y_labelsize)
  return subax


def calc_angles_again(_bout_mat):
  if _bout_mat.shape[-1] == 4:
    _bout_mat_mod = np.insert(_bout_mat, 0, [np.nan,0,0,0], axis=1)
    coord_diffs = np.diff(_bout_mat_mod[...,2:], axis=1)

    _bout_mat[...,0] = np.arctan2(coord_diffs[...,1], coord_diffs[...,0])
    # tailAngles[tailAngles < 0] += 2*np.pi

  return _bout_mat


def main():
  # Free-swimming data
  bf, _, _, umapf = load_bhvr('/media/jamie/2tb-sandisk/2P/bhvr-analysis/total_2rep_raw_modelData.bhvr')
  # Head-fixed data
  bh, _, _, umaph = load_bhvr('/media/jamie/2tb-sandisk/2P/bhvr-analysis/all_sessions_220504.bhvr')

  # Directionality labels
  quad_labels = {1: 'back', 2: 'left', 3: 'straight', 4: 'right'}
  df_labels = LABELS(bf, bh, umapf, umaph)
  df_quads = QUADS(umapf, umaph, quad_labels)

  # Combine data umap data, then split into tail angles (ta) and tail coordinates (tc)
  bout, frame, pos = (df_quads.shape[0], 69, 15)
  umapc = np.concatenate((umapf[:,5:], umaph[:,5:])).reshape(bout, frame, pos, -1)

  # Unhappy with the angles in umapc, calculating new ones
  umapc = np.insert(umapc, 0, np.nan, axis=3)
  for b in range(umapc.shape[0]):
    umapc[b] = calc_angles_again(umapc[b])

  # Extract tail angles (ta) and tail coordinates (tc)
  ta, tc = (umapc[...,0] ,umapc[...,2:])

  # Z-scale combined tail angle data
  zmap = StandardScaler().fit_transform(ta.reshape(bout,-1))

  # PCA analysis
  ncomps = 60
  pca_umap = PCA(n_components=ncomps)
  pcomps = pca_umap.fit_transform(zmap)

  # Build PCA dataframe
  pc_cols = ['pc'+str(i) for i in range(ncomps)]
  df_pca = pd.DataFrame(data=pcomps, columns=pc_cols, dtype=float)

  # Combine into data
  df = pd.concat((df_labels, df_quads, df_pca), axis=1, ignore_index=True)
  df.columns = np.concatenate((df_labels.columns.values,
                               df_quads.columns.values,
                               df_pca.columns.values))

  # Plot PCA separated by session and displacement/heading quadrant
  # Plot overall umap
  ho = df.sample(frac=1, random_state=42)[['session', 'displacement']].apply(tuple, axis=1).unique()
  order = [0,2,6,3,4,5,7,1]

  pl.figure()
  sns.scatterplot(data=df.sample(frac=1, random_state=42), x='pc0', y='pc1', s=6, alpha=1,
                  hue=df.sample(frac=1, random_state=42)[['session', 'displacement']].apply(tuple, axis=1),
                  palette='Paired',
                  hue_order=ho[order])

  # UMAP on prinicple components
  reducer = umap.UMAP(n_neighbors=15, n_components=2, min_dist=0.25, metric='euclidean')
  embedding = reducer.fit_transform(pcomps)

  df_umap = pd.concat((df_labels, df_quads, pd.DataFrame(data=embedding)), axis=1, ignore_index=True)
  df_umap.columns = ['session', 'regular_bout', 'displacement', 'heading', 'u1', 'u2']

  # Plot overall umap
  ho = df_umap.sample(frac=0.5, random_state=42)[['session', 'displacement']].apply(tuple, axis=1).unique()
  pl.figure()
  sns.scatterplot(data=df_umap.sample(frac=0.5, random_state=42), x='u1', y='u2', s=6,
                  hue=df_umap.sample(frac=0.5, random_state=42)[['session', 'displacement']].apply(tuple, axis=1),
                  palette='Paired',
                  hue_order=ho[order])

  # Plot umap separate by session and displacement/heading quadrant
  fig, ax = pl.subplots(1,2)
  sns.scatterplot(data=df_umap.query("session == 'free'").sample(frac=1.0), x='u1', y='u2',
                  hue='displacement', hue_order=quad_labels.values(), s=6,
                  palette="tab10", ax=ax[0])
  sns.scatterplot(data=df_umap.query("session == 'fixed'").sample(frac=1.0), x='u1', y='u2',
                  hue='displacement', hue_order=quad_labels.values(), s=6,
                  palette="tab10", ax=ax[1])
  ax[0].set_xlim([-8, 15]); ax[0].set_ylim([-5, 20]); ax[0].set_title("Free", fontweight='bold')
  ax[1].set_xlim([-8, 15]); ax[1].set_ylim([-5, 20]); ax[1].set_title("Fixed", fontweight='bold')

  # Create videos of certain bouts
  # Range ordering:
  # [1] - Straight & free group
  # [2] - Straight & fixed group
  # [3] - Mixed group, mainly Straight,back & both groups
  # [4] - ???
  # [5] - ???
  u1_range = [ [9.72, 9.95],    [7.65,8], [5.4,6.2],  [-3,1], [1.5,4.5] ]
  u2_range = [ [10.44, 10.56],  [2,2.4],  [3.1,3.7], [2,9],  [5.5,10]  ]

  for n,(u1,u2) in enumerate(zip(u1_range, u2_range)):
    if n == 0:
      ui1 = df_umap['u1'].between(u1[0], u1[1])
      ui2 = df_umap['u2'].between(u2[0], u2[1])
      ubouts = np.nonzero(np.logical_and(ui1, ui2).values)[0]

  boutVideo(umapc, 28230, saveDir, angleAx=1, vidFR=5)
