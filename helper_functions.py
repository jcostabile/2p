# -*- coding: utf-8 -*-

# Load modules
import os
import subprocess
import glob

import numpy as np
import pandas as pd
import matplotlib.pyplot as pl

from scipy.ndimage import median_filter


# Bout detection modified from MH script
def detectBouts(swimMetric, minFramesPerBout, frameRate, loThresh=2, hiThresh=None, zeroLimit=2):
  """
  Detect bouts based on emprically determined characterstics within a trace
  of a swim metric maintaining a certain value.
  """
  #the threshold is determined mostly empirically and probably needs
  #to be changed if our framerate deviates much from 250 - this depends on
  #how aggressively the position traces are smoothed in relation to the
  #framerate

  #bouts should have a clear peak, hence the peak metric should not be
  #maintained for > ~20ms (0.02s)
  maxFramesAtPeak = 0.02/frameRate**-1

  #threshold swim metric
  smetric = swimMetric.copy()
  smetric[np.isnan(smetric)] = 0
  if hiThresh is not None:
    smetric[smetric<loThresh] = 0
    smetric[smetric>hiThresh] = 0
  else:
    smetric[smetric<loThresh] = 0

  #master indexer
  i = 0  ## python like any real language uses 0-based indexing!
  bouts = np.zeros((1,5))  ## '5' because currently tracking five features

  l = np.size(smetric)
  zeroCounter = 0

  while (i < l):
    if smetric[i]==0:
      i += 1
    else:
      # Reset zero counter
      zeroCounter = 0
      #we found a potential "start of bout" - loop till end collecting
      #all speeds
      bStart = i
      b = smetric[i]
      i += 1
      # while(i<l and smetric[i]>0):
      while (i<l and zeroCounter <= zeroLimit):
        if smetric[i] == 0:
          zeroCounter += 1
        else:
          zeroCounter = 0

        b = np.append(b,smetric[i])
        i += 1
      peak = b[b==b.max()]
      #since we loop until smetric==0 the last part 
      #of the bout is in the frame before this frame
      bEnd = i-(zeroLimit+1)

      if (np.size(peak) <= maxFramesAtPeak and np.size(b) >= minFramesPerBout):
        #we have a valid, singular bout
        peakframe = np.nonzero(b==np.max(b))
        peakframe = peakframe[0][0]
        #put peakframe into context
        peakframe = bStart + peakframe
        #a bout should not start on the peakframe
        if(bStart==peakframe):
            bStart = bStart-1
        # bt = np.r_[bStart,peakframe,bEnd,np.sum(b,dtype=float)/frameRate,np.max(b)]
        bt = np.r_[bStart, peakframe, bEnd, np.trapz(b, dx=1/frameRate), np.max(b)]
        bouts = np.vstack((bouts,bt))

  if(np.size(bouts)>1):
    bouts = bouts[1::,:]

  return bouts


# Boxcar filter of size 'window_len', forward and backwards (zero-phase)
def filtfilt(_x, _window_len):
  """
  Performs zero-phase digital filtering with a boxcar filter. Data is first
  passed through the filter in the forward and then backward direction. This
  approach preserves peak location.
  """
  if _x.ndim != 1:
    raise ValueError("filtfilt only accepts 1 dimension arrays.")

  if _x.size < _window_len:
    raise ValueError("Input vector needs to be bigger than window size.")


  if _window_len<3:
    return _x

  #pad signals with reflected versions on both ends
  s=np.r_[_x[_window_len-1:0:-1],_x,_x[-1:-_window_len:-1]]

  #create our smoothing window
  w=np.ones(_window_len,'d')

  #convolve forwards
  y=np.convolve(w/w.sum(),s,mode='valid')

  #convolve backwards
  yret=np.convolve(w/w.sum(),y[-1::-1],mode='valid')

  return yret[-1::-1]


### Applies median filter, averaging, and smoothing in calculation of tail tip
def anglePrep(_cta, _frameRate, _tipLength=(5,-1), _filtWindow=5):

  # Identify locations of missing values, set them equal to negative infinity
  # and apply median filter to tail angles, then set -inf to nan
  nanTA = np.isnan(_cta)
  if nanTA.any():
    _cta[nanTA] = -np.inf
  filtA = median_filter(_cta, size=(3,1), mode='nearest')
  infTA = np.isinf(filtA)
  filtA[infTA] = np.nan
  angles = filtA.copy()

  # Since this is in radians, to average correctly must separate into x,y coordinates
  yavg = np.mean(np.sin(angles[:,_tipLength[0]:_tipLength[1]]), axis=1)
  xavg = np.mean(np.cos(angles[:,_tipLength[0]:_tipLength[1]]), axis=1)

  # Find change in angle using dot-product rule
  u = np.hstack((xavg[:-1,np.newaxis], yavg[:-1,np.newaxis]))
  v = np.hstack((xavg[1:,np.newaxis], yavg[1:,np.newaxis]))
  uv = np.sum(u*v, axis=1)
  # Calculate magnitudes based on arctan(sin,cos)
  diffAngles = np.arctan2(np.cross(u,v), uv)
  diffAngles[np.isnan(diffAngles)] = 0

  # Calculate derivative of angular difference, and filter
  dervDiffAngles = np.abs(diffAngles / _frameRate**-1)
  smoothedMeanAngles = filtfilt(dervDiffAngles, _filtWindow)
  smoothedMeanAngles = np.insert(smoothedMeanAngles, 0, 0)

  return np.insert(diffAngles, 0, 0), smoothedMeanAngles, filtA


### extracts usable (appropriate duration and position) bouts
def boutPrep(camera, bogusFrames, boutData, maxFrame, reps, windowLength=280, shuffle=False):

  # Identify bouts where 'bogusFrames' falls within bout bin
  boutsToRemove, boutsToKeep, tmpDropInds, tmpSwimInds = np.array([]), np.array([]), np.array([]), np.array([])
  leftBinLims, rightBinLims = boutData['startCounter'].values, boutData['endCounter'].values
  for c, (l,r) in enumerate(zip(leftBinLims, rightBinLims)):
    if np.intersect1d(bogusFrames, range(l, r)).size != 0:
      boutsToRemove = np.append(boutsToRemove, c)
      tmpDropInds = np.append(tmpDropInds, np.arange(l, r))
    else:
      boutsToKeep = np.append(boutsToKeep, c)
      tmpSwimInds = np.append(tmpSwimInds, np.arange(l, r))

  # Create bout window for use in neural net
  usableBD = boutData.iloc[boutsToKeep].copy()
  fLength = windowLength/(1000/camera['rate'])  # number of frames, window length in msec
  boutDur = usableBD['endCounter'].values - usableBD['startCounter'].values
  #boutsToUse = np.nonzero(boutDur < fLength)[0]
  # Only the bout data within the radial boundary and under 'windowLength' in time
  #usableBD = usableBD.iloc[boutsToUse]
  framesInBout = np.tile(boutDur, reps)

  ## Begin padding
  counterPad = fLength - framesInBout
  counterPad[counterPad < 0] = 3

  # Randomized padding before bout
  prePad = np.full(counterPad.shape, np.nan)
  prePad[counterPad > 10] = np.array([np.random.randint(10,c) for c in counterPad[counterPad > 10]])
  prePad[counterPad <= 10] = np.array([np.random.randint(c) if c > 0 else 0
                                                            for c in counterPad[counterPad <= 10]])

  # Add padding to lists
  ubs, ube = [], []  # "usable bout starts/ends (counter)"
  ubs.extend((np.tile(usableBD['startCounter'].values, reps) - prePad).tolist())  # "usable bout start (counter)"
  ube.extend((np.tile(ubs, reps) + fLength).tolist())  # "usable bout end (counter)"

  # Handle ranges exceeding 'maxFrame' by reversing the number of frames over
  exInds = np.nonzero(np.asarray(ube) > maxFrame)[0]
  for ex in exInds:
    exDiff = ube[ex] - maxFrame
    ube[ex] = maxFrame - exDiff
    ubs[ex] = ubs[ex] - 2*exDiff

  # Handle ranges with negative indexs
  negInds = np.nonzero(np.asarray(ubs) < 0)[0]
  for neg in negInds:
    negDiff = -1*ubs[neg]
    ubs[neg] = ubs[neg] + negDiff
    ube[neg] = ube[neg] + negDiff

  # Shuffle counters together
  ubPlus = list(zip(map(int, ubs), map(int, ube),
                    map(int, framesInBout.tolist()), map(int, prePad.tolist())))
  if shuffle:  ## Shuffle for real?
    random.seed(a='funStuff')
    random.shuffle(ubPlus)

  return map(list, zip(*ubPlus))


### Determine tail length and positions along it
def tailMeasures(pointSorted):
  ddistances = np.sqrt(np.sum(np.diff(pointSorted, axis=0)**2, axis=1))
  stranged = np.nonzero(ddistances > 100)[0][0]
  if stranged:
    ddistances[stranged:] = np.mean(ddistances[:stranged])
  cumDistance = np.cumsum(ddistances)
  tailLength = cumDistance[-1]
  # print('Total distance: {0:.3f}'.format(tailLength))
  distance = np.insert(cumDistance, 0, 0)/cumDistance[-1]

  return (stranged+1, distance)  ## add '1' because index found with `np.diff`


### Create video of bout from '.MPB' file and frame numbers
def boutVideo(tail, sFrame, eFrame, saveDir, vidFR=30):

  # Dimensions for tail angle arrows
  l, hw, hl = (5,4,3)
  # Plottig region -- currently hard-coded
  acceptableArea = [[50, 550], [200, 550]]

  # Create video of tail movement
  pl.figure(999)
  for i,(tl,ag) in enumerate(zip(tail[sFrame:eFrame, :, 1:], tail[sFrame:eFrame, :, 0])):
      # Plotting
      pl.plot(*tl.T, '.k', markersize=4); pl.ylim(acceptableArea[1]); pl.xlim(acceptableArea[0])
      for t,a in zip(tl[1:], np.cumsum(ag)[1:]):
          pl.arrow(*t, (l)*np.cos(a+np.pi/2), (l)*np.sin(a+np.pi/2),
                    head_width=hw, head_length=hl, fc=None, ec='magenta', linewidth=2)
      pl.text(*(np.asarray(acceptableArea)[0]+5), 'Frame ' + str(i+sFrame), fontsize=10,
              color='red', fontweight='bold')

      # Save plot as .png and clear axis
      pl.savefig(os.path.join(saveDir, 'file%02d.png' % i))
      pl.cla()
  pl.close()

  # Create video from images
  subprocess.call(['ffmpeg', '-framerate', str(vidFR), '-i', os.path.join(saveDir, 'file%02d.png'),
                   '-r', str(30), '-pix_fmt', 'yuv420p', os.path.join(saveDir,
                   'frames-' + str(sFrame) + '-' + str(eFrame) + '.mp4')])
  for file_name in glob.glob(os.path.join(saveDir, '*.png')):
      os.remove(file_name)


def boutPredictions(_mdl_output):
  hw, hl, l = (1,1,2)  ## heading vector lengths
  colrs = ['magenta', 'black']  ## plotting colors

  num_vbouts = len(_mdl_output[0])  ## number of virtual bouts
  if num_vbouts <= 1:
    print('Not enough bouts to print [N = (0,1)], exiting')
    return  ## just exit

  num_subplots = np.ceil(np.sqrt(num_vbouts)).astype(np.int)  ## number of subplots to create
  num_subplots = num_subplots if num_subplots <= 6 else 6

  fig, axs = pl.subplots(num_subplots, num_subplots)
  # Displacement and angle error accural, for a defined set of bouts between 'st' and 'en'
  for bout,ax in zip(range(num_vbouts+1), axs.flatten()[:num_vbouts]):
    st,en = (bout, bout+2) ##num_vbouts+1)
    # Array to update position
    cumulative_pred_pos0 = np.empty(((en-st),2))
    cumulative_pred_pos0[0] = [400,400]
    # Array to update heading
    cumulative_angle = np.empty((en-st))
    cumulative_angle[0] = np.pi/2
    # Loop over bouts
    for bind,i in enumerate(range(st,en-1)):
      # Rotate displacement in direction of heading at the start of the bout
      # pi/2 is subtracted below to convert from "model space" (where the larva heading
      # is wrt to y-axis (i.e., pi/2)) into "real space" (x-axis equals 0 radians)
      ang_shift = -np.pi/2
      c,s = np.cos(cumulative_angle[bind] + ang_shift), np.sin(cumulative_angle[bind] + ang_shift)
      _R = np.array(((c, -s), (s, c)))

      # Update position
      cumulative_pred_pos0[bind+1] = cumulative_pred_pos0[bind] + np.dot(_R, _mdl_output[0][i])
      # Update angle
      cumulative_angle[bind+1] = (cumulative_angle[bind] +
          np.arctan2(_mdl_output[1][i,1], _mdl_output[1][i,0])) % (2*np.pi)

      # Actual plotting section
    ax.plot(*cumulative_pred_pos0[0], colrs[0], marker='d', markersize=6)
    ax.plot(*cumulative_pred_pos0[1], colrs[1], marker='d', markersize=6)
    if bout == 0: ax.legend(['Start', 'End'], frameon=False)
    ax.plot(*cumulative_pred_pos0.T, 'k', linewidth=1, linestyle='--')

    for bind,(ind,colr) in enumerate(zip(range(st,en), colrs)):
      ax.arrow(*cumulative_pred_pos0[bind], l*np.cos(cumulative_angle[bind]), l*np.sin(cumulative_angle[bind]),
          head_width=hw, head_length=hl, fc=colr, ec=colr, linewidth=1)
      ax.set_title("Bout No. {}".format(bout), fontsize=8)

  for ax in axs.flatten():
    ax.axis('off')
  fig.tight_layout(h_pad=0.025, w_pad=0.025)


# Show the swim bouts
def plotSwimBouts(frameIdx, swimMetric, boutStarts, boutEnds, yLabel='Swim Metric'):
    # Overlay on to current plot
    fig, ax = pl.subplots(1,1)
    ax.plot(swimMetric, '.', linestyle='-', color='black')

    ## Highlight swim bouts
    for i0, i1 in zip(boutStarts, boutEnds): 
        ax.plot(frameIdx[i0:i1], swimMetric[i0:i1], linestyle='-', color='violet')

    ax.set_xlabel('Frame No.', fontweight='bold')
    ax.set_ylabel(yLabel, fontweight='bold')


def plotMetric(_B, _plane, _field, _ylabel):

  # Identify bout starts and end
  boutStarts = _B.loc[(_B['bout'] == 1) & (_B['plane'] == _plane)].index.values
  boutEnds = boutStarts + 70

  # Plot metric along with bout locations
  metric = _B.loc[_B['plane'] == _plane][_field].values
  plotSwimBouts(np.arange(metric.shape[0]), metric, boutStarts, boutEnds, _ylabel)


def predictionComparison(_B, _field1, _field2, _label1='Field 1', _label2='Field 2'):

  # Separate bouts based off angular displacement
  rbouts = _B.loc[_B['regular_bout'] == 1]
  ibouts = _B.loc[_B['regular_bout'] == -1]

  # Plot basic metrics
  fig, axs = pl.subplots(2,2)
  axs[0,0].plot(rbouts[_field1], rbouts['dposition'], '.')
  axs[0,1].plot(rbouts[_field2], rbouts['dheading'], '.')
  axs[1,0].plot(ibouts[_field1], ibouts['dposition'], '.')
  axs[1,1].plot(ibouts[_field2], ibouts['dheading'], '.')
  # x labels
  axs[0,0].set_xlabel(_label1)
  axs[1,0].set_xlabel(_label1)
  axs[0,1].set_xlabel(_label2)
  axs[1,1].set_xlabel(_label2)
  # ylabels
  axs[0,0].set_ylabel('Predicted displacement')
  axs[1,0].set_ylabel('Predicted displacement')
  axs[0,1].set_ylabel(r'Predicted $\Delta$heading')
  axs[1,1].set_ylabel(r'Predicted $\Delta$heading')
  fig.tight_layout()
