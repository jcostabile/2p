# -*- coding: utf-8 -*-

"""
The data file is organized by:
  * One row per camera frame @200Hz or @250Hz or other
    --> currently found by calculating the time difference between frames
  * 2 photon behavior camera resolution: calculated to be 90px/mm(formerly thought to be ~85px/mm)
  * Timestamp unit is nanoseconds
  * Camera reference: (0,0) top-left, y increases down, x increases towards the right (see below)
    --> larval tail imaged from bottom, therefore we suspect x increases towards the left


  * Columns contain the information:
    --> <0/1 if scanning is on> <Frame currently scanned on microscope> <Cumulative tail angle> <Timestamp>
    --> This is followed by a triplet of columns for each tailpoint:
      <angle><x-coordinate><y-coordinate> 
      (starting with the base point of the tail which is assumed to be at angle 0)

  * The fish is facing straight down in all cases (therefore the Y-coordinates decrease as you go 
    along the tail)

  * Angles are in radians, successive angles are cumulative

  * If a tail point is lost coordinates will be (0,0) and angle will be 'nan'
  * Lost tail points likely towards end of tail due to image processing reasons

  * Each file contains a separately imaged plane, indexed by the number after the 'Z' in the file
    name. The different text before the 'Z' identifies the session (with each session consisting 
    of multiple z-planes)

  * There is about a 10s gap between imaging planes - so they are imaged in succession but not 
    time-continuous. Each plane should have been imaged for 8.25 minutes.
"""


# Libraries
import os
import gc
import sys
import cv2
import glob
import time
import argparse

import numpy as np
import pandas as pd
import matplotlib.pyplot as pl; pl.ion()

from tifffile import imread
from natsort import natsorted
from experiment import Experiment2P
from image_processing import *

import warnings
warnings.filterwarnings("ignore")


# Ensure input arguments fulfill certain criteria
class _check_values(argparse.Action):
  def __call__(self, parser, namespace, values, option_string=None):
    if self.dest == 'data_directory' or self.dest == 'image_parent_directory':
      if not os.path.exists(values):
        raise argparse.ArgumentError(self, "Directory non-existent")
      setattr(namespace, self.dest, values)


# Handle input arguments
def getOptions(args=sys.argv[1:]):
  # Handle inputs first -- create parser and add arguments
  parser = argparse.ArgumentParser(description="The code extracts tail data from dropped frames saved " +
        "during recordings on the two-photon microscope. In the directory there must be (1) a processed " +
        "'.hdf5' file, (2) the associated '.tail' files, and (3) tif stacks ending with the string " +
        "'_tailImage.tif'. The code will process the tif stacks and save the updated tail information to " +
        "the '.hdf5', along with flags indicating the '.hdf5' was processed () and which frames were updated ()")
  parser.add_argument('--data_directory', type=str, action=_check_values,
        help="Path to directory containing the '.hdf5' file(s); further searches into sub-directories do not" +
        "take place (as is the case with the image directory).")
  parser.add_argument('--image_parent_directory', type=str, action=_check_values,
        help="Path to directory containing 'tailImage.tif' files. It is now considered (via an ad hoc approach) " +
        "that the 'tailImage.tif' files are (or can be) located in a different directory than its associated " +
        "'.hdf5' file. It is assumed that the image files are located in sub-directories and hence the code " +
        "starts with a parent directory and searches through it and its sub-directories. But this need not " +
        "necessarily be the case, as the 'parent directory' could for a folder containing all the 'tailImage.tif' files.")
  options = parser.parse_args(args)
  return options


def tiff_meta_data_table(_data_directory, _image_parent_directory):
  # Identify files to process
  dfiles = []
  for path in glob.glob(os.path.join(_data_directory, '**', '*.hdf5'), recursive=True):
      dfiles.append(path)
  dfiles = sorted(dfiles, key=os.path.basename)

  # Each experiments have their own '.hdf5' file
  filePrefixes = [os.path.basename(os.path.splitext(d)[0]) for d in dfiles]

  # Gather image files ('tailImage.tif') from surrounding directories:
  # code below assumes the image files will be located somewhere in
  # the parent directory
  tfiles = []  ## total image files
  for path in glob.glob(os.path.join(os.path.dirname(_image_parent_directory),
                                     '**','*_tailImage*.tif'), recursive=True):
    tfiles.append(path)
  ifiles = []  ## one image file per plane
  for path in glob.glob(os.path.join(os.path.dirname(_image_parent_directory),
                                     '**','*_tailImage.tif'), recursive=True):
    ifiles.append(path)

  # Dataframe to hold metadata on tiff stacks
  meta_data = pd.DataFrame(columns=['session', 'no_plane', 'no_tiff', 'mean_tiff_size', 'std_tiff_size',
                                     'no_tiff_frames', 'no_dropped_frames', 'tail_data_augmented'])

  # Loop over the '.hdf5' files
  for filepre in filePrefixes:
    # To track all image files associated with session
    total_image_paths = natsorted([t for t in tfiles if os.path.basename(t).startswith(filepre)])

    # For each experiment ('filepre') locate associated tailImage.tif' file and a '.hdf5' file
    image_paths = natsorted([i for i in ifiles if os.path.basename(i).startswith(filepre)])
    hdf5Path = [d for d in dfiles if os.path.basename(d).startswith(filepre)]

    # Track number of frames in tiff stack and number of frames dropped
    plane_tiff_frames = []
    plane_dropped_frames = []

    if len(hdf5Path) == 1:
      # Create experimental data array -- lazy load data from .hdf5
      with Experiment2P(hdf5Path[0]) as exp:
        # Loop over all planes related to file prefix ('filepre')
        for plane in range(len(exp.tail_data)):
          # Extract info about images and tail data
          dat = exp.tail_data[plane].copy()  ## pull tail info from certain plane
          tailinfo = dat[:,4:-1].reshape(dat.shape[0], -1, 3)
          
          # Dropped frames
          inds = np.nonzero(np.any(np.any(np.isnan(tailinfo), axis=2), axis=1))[0]
          plane_dropped_frames.append(len(inds))

          # Tiff frames
          tifStack = imread(image_paths[plane])
          if tifStack.ndim == 3:  ## plane must have dropped frames to be processed
            plane_tiff_frames.append(tifStack[1:].shape[0])  ## first image is irrelevant
          else:  ## 'tifStack' only has one (superfluous) image
            plane_tiff_frames.append(0)

      # Create matrix to build 'meta_data' data table
      meta_dict_append = {
                         'session': filepre,
                         'no_plane': len(exp.tail_data),
                         'no_tiff': len(total_image_paths),
                         'mean_tiff_size': np.mean([os.stat(i).st_size/1e6 for i in total_image_paths]),
                         'std_tiff_size': np.std([os.stat(i).st_size/1e6 for i in total_image_paths]),
                         'no_tiff_frames': [plane_tiff_frames],
                         'no_dropped_frames': [plane_dropped_frames],
                         'tail_data_augmented': exp.tail_data_augmented
                         }

      # Append onto whole data table
      meta_data = meta_data.append(pd.DataFrame(meta_dict_append), ignore_index=True)

  return meta_data


def main(_data_directory, _image_parent_directory):
  # Hard-corded parameters
  testing = False  ## plotting flag
  pxSmoothingArea = (10,10)  ## x,y range to smooth intensity over for tail position recalc

  # Identify files to process
  dfiles = []
  for path in glob.glob(os.path.join(_data_directory, '**', '*.hdf5'), recursive=True):
      dfiles.append(path)
  dfiles = sorted(dfiles, key=os.path.basename)
  # Each experiments have their own '.hdf5' file
  filePrefixes = [os.path.basename(os.path.splitext(d)[0]) for d in dfiles]

  # Gather image files ('tailImage.tif') from surrounding directories:
  # code below assumes the image files will be located somewhere in
  # the parent directory
  ifiles = []  ## image files
  for path in glob.glob(os.path.join(os.path.dirname(_image_parent_directory), '**',
                                     '*_tailImage*.tif'), recursive=True):
    ifiles.append(path)

  # Loop over the '.hdf5' files
  for filepre in filePrefixes:
    start0 = time.perf_counter()

    # For each experiment ('filepre') locate associated tailImage.tif' file and a '.hdf5' file
    imgPaths = natsorted([i for i in ifiles if os.path.basename(i).startswith(filepre)])
    hdf5Path = [os.path.join(_data_directory, d) for d in dfiles if d.startswith(filepre)]

    # Temporary `tail_data` list
    tmp_tail_data = []
    # Records indices that were updated for each plane
    fixed = []

    if len(hdf5Path) == 1:
      # Create experimental data array -- lazy load data from .hdf5
      with Experiment2P(hdf5Path[0]) as exp:

        # Prompt if .hdf5 has been previously updated
        if exp.tail_data_augmented:
            print("\n'{0}' previously updated, skipping...".format(os.path.basename(hdf5Path[0])))
            continue  ## move to next iteration of for loop
            ## Code below may be revisited once behavior reference space has been more fully defined (02/02/2022 - JDC)
            #  while True:
            #    a = input("\n'{0}' previously updated, do you wish to continue? [Y/n] ".format(os.path.basename(hdf5Path[0]))).lower()
            #    if a == "yes" or a == "y" or a == '':
            #      break  ## exit while loop
            #    elif a == "no" or a == "n":
            #      print("'{0}' skipped".format(os.path.basename(hdf5Path[0])))
            #      break  ## exit while loop
            #  if a == "no" or a == "n":
            #    continue  ## move to next iteration of for loop

        # Number of 'imgPaths' must equal number of planes imaged
        if len(imgPaths) != len(exp.tail_data):
            print("\nFor file {}, the number of ".format(os.path.basename(hdf5Path[0])) +
                  "tail image files ({}) does not equal the number ".format(len(imgPaths)) +
                  "of planes imaged ({}), skipping experiment...".format(len(exp.tail_data)))
            continue  ## move to next iteration of for loop

        # Loop over all planes related to file prefix ('filepre')
        for plane in range(len(exp.tail_data)):
          start = time.perf_counter()
          unfixed = []  ## images deemed to remain as poorly tracked per plane

          # Extract info about images and tail data
          dat = exp.tail_data[plane].copy()  ## pull tail info from certain plane
          frameinfo = pd.DataFrame(dat[:,:4], columns=['scan', 'microFrame', 'sumTA', 'ts'])
          tailinfo = dat[:,4:-1].reshape(dat.shape[0], -1, 3)
          # Find dropped frames
          inds = np.nonzero(np.any(np.any(np.isnan(tailinfo), axis=2), axis=1))[0]

          # Load image
          tifStack = imread(imgPaths[plane])
          # Parameter to flip two-photon behavior tail positions along vertical midline
          midline = tifStack.shape[1]/2

          if len(inds) > 0 and tifStack.ndim == 3:  ## plane must have dropped frames to be processed
            images = tifStack[1:]  ## first image is irrelevant
            # Create median image
            median_image = np.median(images, axis=0)
            # "Background" subtracted image
            imgs = np.abs(images.astype(np.float32) - median_image.astype(np.float32)).astype(np.uint8)

            # Force there to be an image for each frame deemed to have a missing data point
            if len(imgs) == len(inds):
              # Position closest to start of larval tail ('chakra')
              chakra = np.mean(tailinfo[:, 0, 1:], axis=0).astype(int)  ## near the swim bladder
              mirror_chakra = np.array([chakra[0] - 2*(chakra[0] - midline), chakra[1]], dtype=int)  ## flip

              # Loop over tif stack associated with 'plane'
              for num, img in enumerate(imgs):
                # Extract tail data
                try:
                    positions, angles, length = tailPositions(img, chakra, pxSmoothingArea, testing=False)
                except (ValueError, IndexError):
                    print("Error in image processing of frame {0} of plane {1} ".format(num, plane) +
                          "in file {}.".format(os.path.basename(imgPaths[plane])))
                    positions, angles, length = (None, None, None)

                # Reinsert information into tail data matrix
                if positions is None:
                  #print(f"Tail length: {length}; Plane number: {plane}; Image number: {num}")
                  unfixed.append(num)
                else:
                  dat[inds[num]] = np.concatenate((frameinfo.iloc[inds[num]].values,
                                                np.hstack((angles[:,None], positions)).ravel(), [np.nan,]))

              # RAM management
              del images, median_image, imgs, img
              del chakra, mirror_chakra
              del positions, angles, length
              gc.collect()

            else:
              print("The number of images in .tif stack ({}) ".format(os.path.basename(imgPaths[plane])) +
                    "does not equal number of frames with missing tail points. Skipping...")
              unfixed = [*range(len(inds))]

              # Manage RAM
              del tifStack, images, imgs, median_image
              gc.collect()

          else:  ## Skipped because 'tifStack' only had one image
            print("No dropped images in .tif stack ({}), skipping...".format(os.path.basename(imgPaths[plane])))
            unfixed = [*range(len(inds))]

            # Manage RAM
            del tifStack
            gc.collect()

          # Mirror all tail positions across verticle midline
          tailinfo2 = dat[:,4:-1].reshape(dat.shape[0], -1, 3).copy()
          tailinfo2[:,:,1] = tailinfo2[:,:,1] - 2*(tailinfo2[:,:,1] - midline)
          # Flip angles too, wait...angles are already flipped in their ref space

          # update 'tail_data'
          tmp_tail_data.append(np.concatenate((frameinfo.values, tailinfo2.reshape(-1,
                               np.prod(tailinfo2.shape[1:])), np.full((dat.shape[0],1), np.nan)), axis=1))

          # append fixed 'tail_data' indices
          fixed_mask = np.ones(len(inds), dtype=bool)
          fixed_mask[unfixed] = False
          fixed.append(inds[fixed_mask])

          stop = time.perf_counter()
          print(f"Plane {plane} processing took {stop - start:.4f} seconds")

          # Manage RAM
          del dat, frameinfo, tailinfo, tailinfo2
          del inds, fixed_mask, unfixed
          gc.collect()

        # Resave to exp
        exp.replaced_tail_frames = fixed
        exp.tail_data_augmented = True
        exp.tail_data = tmp_tail_data
        exp.update_experiment_store()

        # Manage RAM
        del imgPaths, hdf5Path
        del tmp_tail_data, fixed
        gc.collect()
    else:
      print(f"Subject {filepre} skipped...")

    # End loop with print statement
    stop0 = time.perf_counter()
    print(f"Total time for subject {filepre}: {stop0-start0:.4f} seconds")


if __name__ == "__main__":
  testing = True
  if testing:
    bdir = '/media/ac/iDataDrive/RandomWave/processed_data'
    idir = '/media/ac/iDataDrive/RandomWave/raw_data'
    _data_directory, _image_parent_directory = (bdir, idir)
  else:
    # Pull directory information
    opts = getOptions()
    bdir = opts.data_directory
    idir = opts.image_parent_directory

    # Update hdf5 files from directory
    main(bdir, idir)
