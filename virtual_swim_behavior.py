# -*- coding: utf-8 -*-

"""

OUTPUTS:

(1) BEHAVIOR (pandas dataframe)

Format:
behavior = pd.DataFrame(columns=['plane', 'ts', 'bout', 'regular_bout',
                                'dposition', 'dheading', 'sum_tail', 'delta_tail',
                                 'ddelta_tail', 'vigor', 'rolling_vigor', 'rolling_sum_tail',
                                 'rolling_delta_tail'])

Variable list:
  (*) -- Tentative enclusion into neural ANN model
  (X) -- Tentative exclusion from neural ANN model
  (O) -- Not useful for neural ANN model

           'plane' -- plane number                                                   (0)
              'ts' -- timestamps according to 'exp' object                            (0) [seconds]
            'bout' -- location of bout starts                                          (X) [binary]
    'regular_bout' -- flag to indicate bout consists of regular tail movements  (X) [1=Reg, -1=Irr]
       'dposition' -- predicted displacement of bout                                   (X) [pixels]
        'dheading' -- predicted change in heading angle                               (X) [radians]
        'sum_tail' -- summation of tail angles                                       (*)  [radians]
      'delta_tail' -- tail deflection metric                                         (X)  [radians]
     'ddelta_tail' -- abs. value of smoothed tail deflection derivative          (X)  [radians/sec]
           'vigor' -- absolute sum of energy in tail movement                         (X) [unknown]
   'rolling_vigor' -- rolling sum of vigor over the succeeding frames (number = bout window) (*)[U]
'rolling_sum_tail' -- rolling sum of sum_tail over the succeeding frames              (X) [radians]
'rolling_delta_tail' -- rolling sum of delta_tail over the succeeding frames                (X) [U]

(2) Source data file location (string)

(3) Swim trajectory (ANN) model directory location (string)

(4) UMAP (numpy array)

Purpose of array is to assess transfer learning of swim trajectory model. The reason it is
called UMAP is because this was the analytical tool applied to the free and head-fixed swimming
behavior data sets for assessment.

NOTE #1: Tail position 0 = tail tip; tail position -1 = swim bladder
NOTE #2: Coordinates rotated so larva faces positive y axis, angles left unchanged because they
         are relative to adjacent tail position

Format:
  [Rows]: Bouts
  [Cols]: <plane,
          ACTUAL bout Δx displacement, ACTUAL bout Δy displacement,
          ACTUAL bout Δx angle component, ACTUAL bout Δy angle component,
          ~frame 0, tail position 0~ (angle, x coordinate, y coordinate),
          ~frame 0, tail position 1~ (angle, x coordinate, y coordinate),
          ~frame 0, tail position 2~ (angle, x coordinate, y coordinate),
          ...,
          ~frame 0, last tail position~ (angle, x coordinate, ycoordinate),
          ~frame 1, tail position 0~ (angle, x coordinate, y coordinate),
          ~frame 1, tail position 1~ angle, x coordinate, y coordinate,
          ...,
          ~last frame, last tail position~ angle, x coordinate, y coordinate>


NOTES on data input:
The data file is organized by:
  * One row per camera frame @200Hz or @250Hz or other
    --> currently found by calculating the time difference between frames
  * 2 photon behavior camera resolution: calculated to be 90px/mm(formerly thought to be ~85px/mm)
  * Timestamp unit is nanoseconds
  * Camera reference: (0,0) top-left, y increases down, x increases towards the right (see below)
    --> larval tail imaged from bottom, therefore we suspect x increases towards the left

  * Columns contain the information:
    --> <0/1 if scanning is on> <Frame currently scanned on microscope> <Cumulative tail angle> <Timestamp>
    --> This is followed by a triplet of columns for each tailpoint:
      <angle><x-coordinate><y-coordinate> 
      (starting with the base point of the tail which is assumed to be at angle 0)

  * The fish is facing straight down in all cases (therefore the Y-coordinates decrease as you go 
    along the tail)

  * Angles are in radians

  * If a tail point is lost coordinates will be (0,0) and angle will be 'nan'
  * Lost tail points likely towards end of tail due to image processing reasons

  * Each file contains a separately imaged plane, indexed by the number after the 'Z' in the file
    name. The different text before the 'Z' identifies the session (with each session consisting 
    of multiple z-planes)

  * There is about a 10s gap between imaging planes - so they are imaged in succession but not 
    time-continuous. Each plane should have been imaged for 8.25 minutes.
"""


# Add paths
import sys
import h5py
import argparse
import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '3'

import pandas as pd
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as pl; pl.ion()
from natsort import natsorted

from interpolate_missing_data import *
from helper_functions import *
from experiment import Experiment2P

tf.autograph.set_verbosity(0)
tf.get_logger().setLevel('INFO')


class _check_values(argparse.Action):
  def __call__(self, parser, namespace, values, option_string=None):
    if self.dest == 'data_file':
      if not os.path.isfile(values):
        raise argparse.ArgumentError(self, "Data file does not exist, check full path")
      setattr(namespace, self.dest, values)
    if self.dest == 'model_directory':
      if not os.path.isdir(values):
        raise argparse.ArgumentError(self, "Model directory does not exist, check full path")
      setattr(namespace, self.dest, values)


# Handle input arguments
def getOptions(args=sys.argv[1:]):
  # Handle inputs first -- create parser and add the arguments
  parser = argparse.ArgumentParser(description="The code generates a HDF5 file containing both " +
          "extracted and generated (i.e., from swim model) behavioral metrics from head-fixed, " +
          "two-photon neural recording sessions. The HDF5 file is saved in the same directory " +
          "as --data_file with the extension '.bhvr'. For more detail, read the comments at the " +
          "top of this file.")
  parser.add_argument('--data_file', type=str, action=_check_values,
            help="Path to processed (i.e., CaImAn) and updated (i.e., 'update_behavior_hdf5.py') "+
            "HDF5 file of an experiment object (i.e., 'Experiment2P.py').")
  parser.add_argument('--model_directory', type=str, action=_check_values,
            help="Path to directory containing one '.config.txt' file and one '.weights.h5' file "+
            "of an artificial neural network, generated with 'swimBoutModel.py'.")
  options = parser.parse_args(args)
  return options


# Return contents on BHVR file
def load_bhvr(_bhvr_path):

  try:
    B = pd.read_hdf(_bhvr_path)

    with h5py.File(_bhvr_path, 'r') as h5f:
      dataPath = h5f['paths'][0]
      modelPath = h5f['paths'][1]

      if 'umap' in h5f.keys():
        umap = h5f['umap'][:]
        umap_bool = True
      else:
        umap_bool = False

    if umap_bool:
      return B, dataPath, modelPath, umap
    else:
      return B, dataPath, modelPath

  except Exception as e:
    print(e)
    print("Unable to load {}".format(_bhvr_path))


def main(_hdf5Path, _modelPath, _cam_res=90, _tidx=(5,-1)):
  ## Parameters
  camera = {'res': _cam_res}  ## currently hard-coded, two-photon behavior camera resolution

  # NN model paths
  _modelConfigPath = os.path.join(_modelPath, 'config.txt')
  _modelWeightsPath = os.path.join(_modelPath, 'weights.h5')
  # Load tensorflow model
  with open(_modelConfigPath, 'r') as f:
    config = f.readlines()
  model = tf.keras.models.model_from_json(config[0],
                                          custom_objects={'LeakyReLU': tf.keras.layers.LeakyReLU})
  model.load_weights(_modelWeightsPath)

  # Prepare behavior metrics dataframe
  behavior = pd.DataFrame(columns=['plane', 'ts', 'bout', 'regular_bout', 'dposition', 'dheading',
                                   'sum_tail', 'delta_tail', 'ddelta_tail', 'vigor',
                                   'rolling_vigor', 'rolling_sum_tail', 'rolling_delta_tail'])

  # Create experimental data array -- lazy load data from .hdf5
  with Experiment2P(_hdf5Path) as exp:
    if not exp.tail_data_augmented:
      print("Behavior file has not been updated via 'update_behavior_hdf5.py'. This is " +
            "necessary to transform the tail coordinates into the appropriate space. Exiting...")
      return

    # If file has been updated (and thus transformed into appropriate space)
    for plane in range(len(exp.tail_data)):
      dat = exp.tail_data[plane]
      # Make arrays
      frameinfo = pd.DataFrame(dat[:,:4], columns=['scan', 'microFrame', 'sumTA', 'ts'])
      tailinfo = dat[:,4:-1].reshape(dat.shape[0], -1, 3)
      ts = frameinfo['ts'].values
      dts = (ts[1] - ts[0])/1e9  ## nanosecond
      frameRate = np.round(1/dts)  ## Hertz
      camera['rate'] = frameRate

      # Interpolate over 'n' (currently 5) frames to estimate missing data
      tailinfo = run_interpolation(tailinfo.copy(), 5, _tidx)

      # Convert tail angles into a metric ('ddelta_tail') to identify bout beginnings and ends
      cta = np.cumsum(tailinfo[:,:,0], axis=1)
      delta_tail, ddelta_tail, filt_cta = anglePrep(cta.copy(), frameRate, _tidx)

      # Detect bouts using functions from 'temlr.py'
      avgThresh = lambda a: np.mean(a)+1.5*np.std(a) if (np.mean(a)+1.5*np.std(a)) > 8 else 8
      bouts = detectBouts(ddelta_tail, 20, frameRate, loThresh=avgThresh(ddelta_tail), zeroLimit=3)
      boutdata = pd.DataFrame(bouts, columns=['startCounter', 'peakCounter', 'endCounter', 'auc', 'max'])
      boutdata = boutdata.astype({'startCounter': int, 'peakCounter': int, 'endCounter': int})

      # Move on if there are no bouts
      if boutdata.shape[0] == 0:
        print("No bouts detected in plane {0} for {1}, skipping plane...".format(plane,
               os.path.basename(_hdf5Path)))
        continue

      # Exclude bouts with NaNs between their starts and stops;
      # and to identify bouts of correct length
      lostTail = np.nonzero(np.any(np.isnan(filt_cta[:,_tidx[0]:_tidx[1]]), 1))[0]
      try:
        boutStarts, boutEnds, _, _ = boutPrep(camera, lostTail, boutdata, tailinfo.shape[0], reps=1)
      except ValueError:
        print("All bouts (N={0}) detected in plane {1} have a ".format(boutdata.shape[0], plane) +
              "missing frame for {0}, skipping plane...".format(os.path.basename(_hdf5Path)))
        continue

      # Indices of bouts with padding
      indexes = np.array([np.arange(bs,be) for bs,be in zip(boutStarts, boutEnds)])

      # This portion of the code will generate missing tail information in the EXTENDED
      # frame window, i.e., when the bout window is increased to 70 frames as input to the ANN
      tailinfo = interpolate_using_avg_coord_diff(tailinfo, indexes, 5)

      # Update variables with "added" data to 'tailinfo'
      cta = np.cumsum(tailinfo[:,:,0], axis=1)
      delta_tail, ddelta_tail, filt_cta = anglePrep(cta.copy(), frameRate, _tidx)

      # Assemble data for neural network ('Rds' and 'dstail'), excluding last frame
      ## tail angles
      ta = filt_cta[indexes,_tidx[0]:_tidx[1]]
      dstail = np.stack((np.cos(ta[:,:-1]), np.sin(ta[:,:-1])), axis=-1)
      ## tail positions
      origin = tailinfo[indexes,0,1:]  ## position of "swim bladder", to translate other points around
      coords = tailinfo[indexes,_tidx[0]:_tidx[1],1:]
      Rds = coords[:,:-1] - origin[:,:-1][:,:,np.newaxis]

      # Ensure there are no NaNs
      if np.isnan(dstail).any() or np.isnan(Rds).any():
        print("NaN values detected ANN inut ('dstail' & 'Rds') in plane {0} ".format(plane) +
              "for missing frame(s) for {0}, skipping plane...".format(os.path.basename(_hdf5Path)))
        continue

      # Scale tail position from 2P resolution (90 px/mm) to behavior rig resolution (8 px/mm)
      Rds = Rds * (8/camera['res'])

      # Fit model to new data
      # REMEMBER, 'resultY' is a list of two elements:
      # (1) x,y displacement, (2) x,y angular components
      resultY = model.predict(x = {"displacement_input": Rds, "angular_input": dstail})

      # Distribution of inter-bout interval (IBI) in seconds -- calculated but unused
      bts = (np.asarray(boutStarts[1:]) - np.asarray(boutEnds[:-1]))/camera['rate']

      ## Build UMAP data = combined model inputs / outputs into array
      mdl_inputs = np.concatenate((ta[:,:-1][...,None], Rds), axis=3).reshape(ta.shape[0],-1)
      plane_array = plane*np.ones((ta.shape[0],))[:,None]
      if plane == 0:
        umap_array = np.concatenate((plane_array, resultY[0], resultY[1], mdl_inputs), axis=1)
      else:
        umap_array = np.vstack((umap_array, np.concatenate((plane_array, resultY[0], resultY[1],
                                                            mdl_inputs), axis=1)))

      ## Build array to append onto 'behavior' data frame
      frames_in_plane, frames_in_bout = (dat.shape[0], boutEnds[0] - boutStarts[0])
      bout_occurances = np.zeros(frames_in_plane)
      dposition = np.zeros(frames_in_plane)
      dheading = np.zeros(frames_in_plane)
      ang_disp = np.zeros(frames_in_plane)

      # '1' where bouts began
      bout_occurances[boutStarts] = 1

      # Model prediction metrics
      ## predicted displacement from model
      dposition[boutStarts] = np.sqrt(resultY[0][:,0]**2 + resultY[0][:,1]**2)
      ## predicted change in heading
      dheading[boutStarts] = np.arctan2(resultY[1][:,1], resultY[1][:,0])
      ## predicted direction of displacement, is bout "forwards" or "backwards"
      ang_disp[boutStarts] = np.arctan2(resultY[0][:,1], resultY[0][:,0])
      reg_bout = np.sign(ang_disp).astype(int)  ## bout identified with a backward trajectory

      # Vigor metrics
      convolve_start, convolve_stop = (int(frames_in_bout/2), -1*(int(frames_in_bout/2)-1))
      vigor = np.convolve(ddelta_tail, dts*np.ones(frames_in_bout),
                          mode='full')[convolve_start:convolve_stop]

      # Identify captured tail angle
      tail_pos_nan = np.append(np.nonzero(~np.isnan(filt_cta))[1], 0)
      last_tail_pos_nan = np.nonzero(np.diff(tail_pos_nan) != 1)[0]
      last_tail = tail_pos_nan[last_tail_pos_nan]

      # Create matrix and append to 'behavior' data frame
      behavior_append = pd.DataFrame(data=np.column_stack((
                            plane*np.ones((frames_in_plane,)),                ## 'plane'
                            exp.tail_frame_times[plane],                      ## 'ts'
                            bout_occurances,                                  ## 'bout'
                            reg_bout,                                         ## 'regular_bout'
                            dposition,                                        ## 'dposition'
                            dheading,                                         ## 'dheading'
                            filt_cta[range(filt_cta.shape[0]), last_tail],    ## 'sum_tail'
                            delta_tail,                                       ## 'delta_tail'
                            ddelta_tail,                                      ## 'ddelta_tail'
                            vigor                                             ## 'vigor'
                            )), columns=behavior.columns[:-3])

      # Temporarily add a bouts worth of info to end
      behavior_append = pd.concat([behavior_append, behavior_append.sample(n=frames_in_bout)],
                                  ignore_index=True)

      # Include rolling window calcs
      behavior_append['rolling_vigor'] = behavior_append.rolling(frames_in_bout)['vigor'].sum().shift(-69)
      behavior_append['rolling_sum_tail'] = behavior_append.rolling(frames_in_bout)['sum_tail'].sum().shift(-69)
      behavior_append['rolling_delta_tail'] = behavior_append.rolling(frames_in_bout)['delta_tail'].sum().shift(-69)

      # Reduce rows
      behavior_append = behavior_append.iloc[:-frames_in_bout]

      # Append onto data frame
      behavior = pd.concat([behavior, behavior_append], ignore_index=True)

  # Convert dtypes to int for some columns
  for col in behavior.columns[[0,2,3]]:  ## plane, bout, regular_bout
    behavior[col] = behavior[col].astype(int)

  # Convert dtypes to int for other columns
  for col in behavior.columns[[1,4,5,6,7,8,9,10,11,12]]:  ## all the others...
    behavior[col] = behavior[col].astype(float)

  return behavior, umap_array


if __name__ == "__main__":
  testing, local = (True, True)
  # Examples for debugging
  if testing:
    if local:
      hdf5Path = '/media/jamie/2tb-sandisk/2P/210818_1_2/newHDF5/Fish01_GCaMP_vlgut_FBd_5dpf_RandomWave.hdf5'
      modelPath = '/home/jamie/projects/basic-track/oop-models/free_swimming_model/'
    else:
      hdf5Path = '/media/ac/iDataDrive/RandomWave/First_Full_Analysis/Fish02_GCaMP_vlgut_FBv_5dpf_RandomWave.hdf5'
      modelPath = '/media/ac/iDataDrive/RandomWave/First_Full_Analysis/free_swimming_model/'

  # Run behavior extraction code
  if not testing:
    # Pull user inputs
    opts = getOptions()
    hdf5Path = opts.data_file
    modelPath = opts.model_directory

    # Generate save file location
    saveFilePath = os.path.splitext(hdf5Path)[0] + '.bhvr'

    B, UMAP = main(hdf5Path, modelPath)

    # Save to HDF5
    try:
      if B is not None:
        B.to_hdf(saveFilePath, key='B', mode='w')

        # Append paths for HDF5 object
        dt = h5py.special_dtype(vlen=str)
        path_len = sorted([len(hdf5Path), len(modelPath)], reverse=True)[0]
        with h5py.File(saveFilePath, 'r+') as h5f:
          dset = h5f.create_dataset('paths', (path_len,), dtype=dt)
          dset[0] = hdf5Path
          dset[1] = modelPath

        # Add 'UMAP' array to hdf5 object
        with h5py.File(saveFilePath, 'r+') as h5f:
          h5f.create_dataset('umap', data=UMAP)

        print("File created: {}".format(saveFilePath))
    except:
      print("Unable to create {}".format(saveFilePath))
